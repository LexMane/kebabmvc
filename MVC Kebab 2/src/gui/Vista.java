package gui;

import base.enums.TipoCarne;
import base.enums.Puesto;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame {
    private final static String TITULOFRAME="Aplicación Varias Tablas";
    private JTabbedPane tabbedPane;
    private JPanel panel1;
    private JPanel JPanelKebab;
    private JPanel JPanelEmpleado;
    private JPanel JPanelLocal;

    //KEBAB
    JButton anadirKebab;
    JButton modificarKebab;
    JButton eliminarKebab;
    JComboBox comboLocalKebab;
    JTextField txtNombreProducto;
    JComboBox comboCarne;
    DatePicker fechaPedido;
    JTextField txtPrecio;
    JTable kebabTabla;
    JCheckBox salsaCheckBox;
    JCheckBox verduraCheckBox;
    JCheckBox patatasCheckBox;

    //EMPLEADOS

    JTextField txtNombreEmpleado;
    JComboBox comboPuesto;
    JTable empleadosTabla;
    DatePicker fechaNacimiento;
    JTextField txtSalario;
    JButton eliminarEmpleado;
    JButton anadirEmpleado;
    JButton modificarEmpleado;

    //LOCAL
    JTextField txtNombreLocal;
    JTextField txtCiudad;
    JTextField txtCodigopostal;
    JTable localesTabla;
    JComboBox comboLocalEmpleado;
    JComboBox comboEmpleado;
    JButton eliminarLocal;
    JButton anadirLocal;
    JButton modificarLocal;

    //BUSQUEDA
    JLabel etiquetaEstado;

    //DEFAULT TABLE MODEL
    DefaultTableModel dtmEmpleados;
    DefaultTableModel dtmLocales;
    DefaultTableModel dtmKebab;

    //BARRA MENU
    JMenuItem itemOpciones;
    JMenuItem itemConectar;

    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

    //CUADRO DIALOGO
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    public void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200,this.getHeight()+100));
        this.setLocationRelativeTo(this);
        //creo cuadro de dialogo
        optionDialog=new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    private void setTableModels() {
        this.dtmKebab =new DefaultTableModel();
        this.kebabTabla.setModel(dtmKebab);
        this.dtmLocales =new DefaultTableModel();
        this.localesTabla.setModel(dtmLocales);
        this.dtmEmpleados =new DefaultTableModel();
        this.empleadosTabla.setModel(dtmEmpleados);
    }

    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        //por cada item que tenga funcionalidad tiene un ActionCommand
        itemOpciones= new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemConectar= new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemDesconectar= new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir=new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemConectar);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        //centrar en horizontal
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    private void setEnumComboBox() {
        for (Puesto constant: Puesto.values()) {
            comboPuesto.addItem(constant.getValor());
        }
        comboPuesto.setSelectedIndex(-1);
        for (TipoCarne constant: TipoCarne.values()) {
            comboCarne.addItem(constant.getValor());
        }
        comboCarne.setSelectedIndex(-1);
    }

    private void setAdminDialog() {
        btnValidate= new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword= new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100,26));
        Object[] options = new Object[] {adminPassword,btnValidate};
        JOptionPane jop =new JOptionPane("Introduce la contraseña",
                JOptionPane.WARNING_MESSAGE,JOptionPane.YES_NO_OPTION,null,options);
        adminPasswordDialog=new JDialog(this,"Opciones",true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

}
