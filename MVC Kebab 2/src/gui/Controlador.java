package gui;

import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

/**
 * Created by Alex Nae on 13/01/2022.
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    private void refrescarTodo() {
        refrescarLocales();
        refrescarEmpleado();
        refrescarKebabs();
        refrescar = false;
    }

    private void addActionListeners(ActionListener listener) {
        vista.anadirKebab.addActionListener(listener);
        vista.anadirKebab.setActionCommand("anadirKebab");
        vista.anadirEmpleado.addActionListener(listener);
        vista.anadirEmpleado.setActionCommand("anadirEmpleado");
        vista.anadirLocal.addActionListener(listener);
        vista.anadirLocal.setActionCommand("anadirLocal");
        vista.eliminarKebab.addActionListener(listener);
        vista.eliminarKebab.setActionCommand("eliminarKebab");
        vista.eliminarEmpleado.addActionListener(listener);
        vista.eliminarEmpleado.setActionCommand("eliminarEmpleado");
        vista.eliminarLocal.addActionListener(listener);
        vista.eliminarLocal.setActionCommand("eliminarLocal");
        vista.modificarKebab.addActionListener(listener);
        vista.modificarKebab.setActionCommand("modificarKebab");
        vista.modificarEmpleado.addActionListener(listener);
        vista.modificarEmpleado.setActionCommand("modificarEmpleado");
        vista.modificarLocal.addActionListener(listener);
        vista.modificarLocal.setActionCommand("modificarLocal");
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
        vista.itemDesconectar.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    private void addItemListeners(Controlador controlador) {
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     *
     * @param e Evento producido en una lista
     */

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.empleadosTabla.getSelectionModel())) {
                int row = vista.empleadosTabla.getSelectedRow();
                vista.txtNombreEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 1)));
                vista.fechaNacimiento.setDate((Date.valueOf(String.valueOf(vista.empleadosTabla.getValueAt(row, 2)))).toLocalDate());
                vista.comboLocalEmpleado.setSelectedItem(String.valueOf(vista.empleadosTabla.getValueAt(row, 3)));
                vista.comboPuesto.setSelectedItem(String.valueOf(vista.empleadosTabla.getValueAt(row, 4)));
                vista.txtSalario.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 5)));
            } else if (e.getSource().equals(vista.localesTabla.getSelectionModel())) {
                int row = vista.localesTabla.getSelectedRow();
                vista.txtNombreLocal.setText(String.valueOf(vista.localesTabla.getValueAt(row, 1)));
                vista.txtCiudad.setText(String.valueOf(vista.localesTabla.getValueAt(row, 2)));
                vista.txtCodigopostal.setText(String.valueOf(vista.localesTabla.getValueAt(row, 3)));
            } else if (e.getSource().equals(vista.kebabTabla.getSelectionModel())) {
                int row = vista.kebabTabla.getSelectedRow();
                vista.txtNombreProducto.setText(String.valueOf(vista.kebabTabla.getValueAt(row, 1)));
                vista.comboLocalKebab.setSelectedItem(String.valueOf(vista.kebabTabla.getValueAt(row, 2)));
                vista.comboEmpleado.setSelectedItem(String.valueOf(vista.kebabTabla.getValueAt(row, 3)));
                vista.comboCarne.setSelectedItem(String.valueOf(vista.kebabTabla.getValueAt(row, 4)));
                vista.fechaPedido.setDate((Date.valueOf(String.valueOf(vista.kebabTabla.getValueAt(row, 5)))).toLocalDate());
                vista.txtPrecio.setText(String.valueOf(vista.kebabTabla.getValueAt(row, 9)));
            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.empleadosTabla.getSelectionModel())) {
                    borrarCamposEmpleado();
                } else if (e.getSource().equals(vista.localesTabla.getSelectionModel())) {
                    borrarCamposLocal();
                } else if (e.getSource().equals(vista.kebabTabla.getSelectionModel())) {
                    borrarCamposKebab();
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;

            case "Conectar":
                modelo.conectar();
                break;

            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(),
                        vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()),
                        String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Vista(), new Modelo());
                break;
            case "anadirKebab":
                try {
                    if (comprobarKebabVacio()) {
                        Util.showErrorAlert("Rellena todos los campos de Kebab");
                        vista.kebabTabla.clearSelection();
                    } else if (modelo.productoYaExiste(vista.txtNombreProducto.getText())) {
                        Util.showErrorAlert("Ese Producto ya existe");
                        vista.kebabTabla.clearSelection();
                    } else {
                        modelo.insertarKebab(
                                vista.txtNombreProducto.getText(),
                                Integer.valueOf((String) vista.comboLocalEmpleado.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fechaPedido.getDate(),
                                (String) vista.comboCarne.getSelectedItem(),
                                vista.salsaCheckBox.isSelected(),
                                vista.verduraCheckBox.isSelected(),
                                vista.patatasCheckBox.isSelected(),
                                Integer.valueOf((String) vista.comboEmpleado.getSelectedItem()));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.kebabTabla.clearSelection();
                }
                borrarCamposKebab();
                refrescarKebabs();
                break;
            case "modificarKebab":
                try {
                    if (comprobarKebabVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.kebabTabla.clearSelection();
                    } else {
                        modelo.modificarKebab(
                                vista.txtNombreProducto.getText(),
                                Integer.valueOf((String) vista.comboLocalKebab.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fechaPedido.getDate(),
                                (String) vista.comboCarne.getSelectedItem(),
                                vista.salsaCheckBox.isSelected(),
                                vista.verduraCheckBox.isSelected(),
                                vista.patatasCheckBox.isSelected(),
                                Integer.valueOf((String) vista.comboEmpleado.getSelectedItem()),
                                Integer.parseInt((String) vista.kebabTabla.getValueAt(vista.kebabTabla.getSelectedRow(), 0)));

                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.kebabTabla.clearSelection();
                }
                borrarCamposKebab();
                refrescarKebabs();
                break;
            case "eliminarKebab":
                modelo.borrarKebab(Integer.parseInt((String) vista.kebabTabla.getValueAt(vista.kebabTabla.getSelectedRow(), 0)));
                borrarCamposKebab();
                refrescarKebabs();
                break;
            case "anadirLocal": {
                try {
                    if (comprobarLocalVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.localesTabla.clearSelection();
                    } else if (modelo.localNombreYaExiste(vista.txtNombreLocal.getText(),
                            vista.txtCiudad.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un nombre de local diferente");
                        vista.localesTabla.clearSelection();
                    } else {
                        modelo.insertarLocal(vista.txtNombreLocal.getText(),
                                vista.txtCiudad.getText(),
                                vista.txtCodigopostal.getText());
                        refrescarLocales();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.localesTabla.clearSelection();
                }
                borrarCamposLocal();
            }
            break;
            case "modificarLocal": {
                try {
                    if (comprobarLocalVacio()) {
                        Util.showErrorAlert("Rellena todos los campos de Local");
                        vista.localesTabla.clearSelection();
                    } else {
                        modelo.modificarLocal(vista.txtNombreLocal.getText(), vista.txtCiudad.getText(),
                                vista.txtCodigopostal.getText(),
                                Integer.parseInt((String) vista.localesTabla.getValueAt(vista.localesTabla.getSelectedRow(), 0)));
                        refrescarLocales();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.localesTabla.clearSelection();
                }
                borrarCamposLocal();
            }
            break;
            case "eliminarLocal":
                modelo.borrarLocal(Integer.parseInt((String) vista.localesTabla.getValueAt(vista.localesTabla.getSelectedRow(), 0)));
                borrarCamposLocal();
                refrescarLocales();
                break;
            case "anadirEmpleado": {
                try {
                    if (comprobarEmpleadoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.empleadosTabla.clearSelection();
                    } else if (modelo.empleadoNombreYaExiste(vista.txtNombreEmpleado.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un nombre de empleado diferente.");
                        vista.empleadosTabla.clearSelection();
                    } else {
                        modelo.insertarEmpleado(

                                vista.txtNombreEmpleado.getText(),
                                Integer.valueOf((String) vista.comboLocalEmpleado.getSelectedItem()),
                                vista.fechaNacimiento.getDate(),
                                Double.valueOf(vista.txtSalario.getText()),
                        (String) vista.comboPuesto.getSelectedItem());
                        refrescarEmpleado();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.empleadosTabla.clearSelection();
                }
                borrarCamposEmpleado();
            }
            break;
            case "modificarEmpleado": {
                try {
                    if (comprobarEmpleadoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.empleadosTabla.clearSelection();
                    } else {
                        modelo.modificarEmpleado( vista.txtNombreEmpleado.getText(),
                                Integer.valueOf((String) vista.comboLocalKebab.getSelectedItem()),
                                vista.fechaNacimiento.getDate(),
                                Double.valueOf(vista.txtSalario.getText()),
                                (String) vista.comboPuesto.getSelectedItem(),
                                Integer.parseInt((String) vista.empleadosTabla.getValueAt(vista.empleadosTabla.getSelectedRow(), 0)));
                        refrescarEmpleado();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.empleadosTabla.clearSelection();
                }
                borrarCamposEmpleado();
            }
            break;
            case "eliminarEmpleado":
                modelo.borrarEmpleado(Integer.parseInt((String) vista.empleadosTabla.getValueAt(vista.empleadosTabla.getSelectedRow(), 0)));
                borrarCamposEmpleado();
                refrescarEmpleado();
                break;
        }
    }

    private void refrescarEmpleado() {
        try {
            vista.empleadosTabla.setModel(construirTableModelEmpleados(modelo.consultarEmpleado()));
            vista.comboEmpleado.removeAllItems();
            for (int i = 0; i < vista.dtmEmpleados.getRowCount(); i++) {
                vista.comboEmpleado.addItem(vista.dtmEmpleados.getValueAt(i, 0));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelEmpleados(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmEmpleados.setDataVector(data, columnNames);
        return vista.dtmEmpleados;
    }

    private void refrescarLocales() {
        try {
            vista.localesTabla.setModel(construirTableModelLocales(modelo.consultarLocal()));
            vista.comboLocalEmpleado.removeAllItems();
            for (int i = 0; i < vista.dtmLocales.getRowCount(); i++) {
                vista.comboLocalEmpleado.addItem(vista.dtmLocales.getValueAt(i, 0));
                vista.comboLocalKebab.addItem(vista.dtmLocales.getValueAt(i, 0));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelLocales(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmLocales.setDataVector(data, columnNames);
        return vista.dtmLocales;
    }

    /**
     * Actualiza los libros que se ven en la lista y los comboboxes
     */
    private void refrescarKebabs() {
        try {
            vista.kebabTabla.setModel(construirTableModelKebab(modelo.consultarKebab()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelKebab(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmKebab.setDataVector(data, columnNames);
        return vista.dtmKebab;

    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    private void setOptions() {
    }

    private void borrarCamposKebab() {
        vista.comboEmpleado.setSelectedIndex(-1);
        vista.comboLocalEmpleado.setSelectedIndex(-1);
        vista.txtNombreProducto.setText("");
        vista.comboCarne.setSelectedIndex(-1);
        vista.txtPrecio.setText("");
        vista.fechaPedido.setText("");
    }

    private void borrarCamposLocal() {
        vista.txtNombreLocal.setText("");
        vista.txtCiudad.setText("");
        vista.txtCodigopostal.setText("");
    }

    private void borrarCamposEmpleado() {
        vista.txtNombreEmpleado.setText("");
        vista.fechaNacimiento.setText("");
        vista.comboPuesto.setSelectedIndex(-1);
        vista.txtSalario.setText("");
    }

    private boolean comprobarKebabVacio() {
        return vista.txtNombreProducto.getText().isEmpty() ||
                vista.txtPrecio.getText().isEmpty() ||
                vista.comboCarne.getSelectedIndex() == -1 ||
                vista.comboLocalKebab.getSelectedIndex() == -1 ||
                vista.comboEmpleado.getSelectedIndex() == -1 ||
                vista.fechaPedido.getText().isEmpty();
    }

    private boolean comprobarLocalVacio() {
        return vista.txtCiudad.getText().isEmpty() ||
                vista.txtNombreLocal.getText().isEmpty() ||
                vista.txtCodigopostal.getText().isEmpty();
    }

    private boolean comprobarEmpleadoVacio() {
        return vista.txtNombreEmpleado.getText().isEmpty() ||
                vista.txtSalario.getText().isEmpty() ||
                vista.fechaNacimiento.getText().isEmpty() ||
                vista.comboPuesto.getSelectedIndex() == -1 ||
                vista.comboLocalEmpleado.getSelectedIndex() == -1;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
