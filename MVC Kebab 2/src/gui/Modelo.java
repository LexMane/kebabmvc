package gui;

import base.enums.TipoCarne;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

/**
 * Created by Alex Nae on 13/01/2022.
 */
public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public String getIp() {
        return ip;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public Modelo() {
        getPropValues();
    }

    private Connection conexion;

    void conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://"
                    + ip + ":3306/kebabs", user, password);
        } catch (SQLException e) {
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://"
                        + ip + ":3306/", user, password);
                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();
            } catch (SQLException | IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    private String leerFichero() throws IOException {
        //basedatos_java no tiene delimitador
        //StringBuilder es dinamica
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }

    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void insertarLocal(String nombreLocal, String ciudad, String codigo_postal) {
        String sentenciaSql = "INSERT INTO `local`(`nombre_local`, `ciudad`, `codigo_postal`)" +
                "VALUES (?,?,?)";

        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombreLocal);
            sentencia.setString(2, ciudad);
            sentencia.setString(3, codigo_postal);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    void insertarEmpleado(String nombre_empleado, int id_local, LocalDate fecha_nacimiento, double salario, String puesto) {
        String sentenciaSql = "INSERT INTO `empleado`(`nombre_empleado`,`id_local`, `fecha_nacimiento`, `salario`, `puesto`) VALUES (?, ?, ?,?,?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre_empleado);
            sentencia.setInt(2, id_local);
            sentencia.setDate(3, Date.valueOf(fecha_nacimiento));
            sentencia.setDouble(4, salario);
            sentencia.setString(5,puesto);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void insertarKebab(String nombreProducto, int id_local, double precio, LocalDate fecha_pedido, String tipo_carne, boolean salsa, boolean verdura, boolean patatas, int id_empleado) {
        String sentenciaSql = "INSERT INTO `kebab`(`nombre_producto`, `id_local`, `precio`, `fecha_pedido`, `tipo_carne`, `salsa`, `verdura`, `patatas`, `id_empleado`)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        //int idempleado = Integer.valueOf(editorial.split(" ")[0]);
        //int idautor = Integer.valueOf(autor.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1,nombreProducto);
            sentencia.setInt(2, id_local);
            sentencia.setDouble(3, precio);
            sentencia.setDate(4, Date.valueOf(fecha_pedido));
            sentencia.setString(5, tipo_carne);
            sentencia.setBoolean(6, salsa);
            sentencia.setBoolean(7, verdura);
            sentencia.setBoolean(8, patatas);
            sentencia.setInt(9, id_empleado);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarLocal(String nombreLocal, String ciudad, String codigo_postal, int i) {
        String sentenciaSql = "UPDATE `local` SET `nombre_local`=?,`ciudad`=?,`codigo_postal`=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombreLocal);
            sentencia.setString(2, ciudad);
            sentencia.setString(3, codigo_postal);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarEmpleado( String nombre_empleado, int id_local, LocalDate fecha_nacimiento, double salario, String puesto, int id_empleado) {

        String sentenciaSql = "UPDATE `empleado` SET `nombre_empleado`=?, `id_local`=?,`fecha_nacimiento`=?,`salario`=?, `puesto`=? where `id_empleado`=?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre_empleado);
            sentencia.setInt(2, id_local);
            sentencia.setDate(3, Date.valueOf(fecha_nacimiento));
            sentencia.setDouble(4, salario);
            sentencia.setString(5,puesto);
            sentencia.setInt(6, id_empleado);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarKebab(String nombreProducto, int id_local, double precio, LocalDate fecha_pedido, String tipo_carne, boolean salsa, boolean verdura, boolean patatas, int id_empleado, int id_producto) {

        String sentenciaSql = "UPDATE `kebab` SET `nombre_producto`=?,`id_local`=?,`precio`=?,`fecha_pedido`=?," +
                "`tipo_carne`=?,`salsa`=?,`verdura`=?,`patatas`=?,`id_empleado`=? WHERE id_producto =?";

        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombreProducto);
            sentencia.setInt(2, id_local);
            sentencia.setDouble(3,precio);
            sentencia.setDate(4, Date.valueOf(fecha_pedido));
            sentencia.setString(5, tipo_carne);
            sentencia.setBoolean(6, salsa);
            sentencia.setBoolean(7, verdura);
            sentencia.setBoolean(8, patatas);
            sentencia.setInt(9,id_empleado);
            sentencia.setInt(10,id_producto);

            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void borrarEmpleado(int id_empleado) {
        String sentenciaSql = "DELETE FROM Empleado WHERE id_empleado=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id_empleado);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void borrarLocal(int id_local) {
        String sentenciaSql = "DELETE FROM Local WHERE id_local = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id_local);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void borrarKebab(int id_producto) {
        String sentenciaSql = "DELETE FROM Kebab WHERE id_producto = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id_producto);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    ResultSet consultarEmpleado() throws SQLException {
        String sentenciaSql = "SELECT concat(id_empleado) AS 'ID', concat(nombre_empleado) AS 'Nombre Empleado', " +
                "concat(id_local) AS 'ID Local', concat(fecha_nacimiento) AS 'Fecha Nacimiento',concat(salario) AS 'Salario'," +
                "concat(puesto) AS 'Puesto' FROM Empleado";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    ResultSet consultarLocal() throws SQLException {
        String sentenciaSql = "SELECT concat(id_local) AS 'ID', concat(nombre_local) AS 'Nombre Local', concat(ciudad) AS 'Ciudad', " +
                "concat(codigo_postal) AS 'Codigo Postal' FROM Local";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    ResultSet consultarKebab() throws SQLException {
        String sentenciaSql = "SELECT concat(b.id_producto) AS 'ID', concat(b.nombre_producto) AS 'Nombre Producto'," +
                " concat(l.id_local) AS 'ID Local', " +
                "concat(b.precio) AS 'Precio', concat(b.fecha_pedido) AS 'Fecha Pedido', " +
                "concat(b.tipo_carne) AS 'Tipo de Carne', " +
                "concat(b.salsa) AS 'Salsa', concat(b.verdura) AS 'Verdura'," +
                "concat(b.patatas) AS 'Patatas',"+
                "concat(e.id_empleado) AS 'ID Empleado'"+
                " FROM kebab AS b " +
                "INNER JOIN empleado AS e ON e.id_empleado = b.id_empleado INNER JOIN " +
                "Local AS l ON l.id_local=b.id_local";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    //usamos los datos del cuadro de dialogo
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.ip=ip;
        this.user=user;
        this.password=pass;
        this.adminPassword=adminPass;
    }

    //comprobaciones llamando a funciones de sql
    public boolean productoYaExiste(String nombreProducto) {
        String consulta="SELECT existeProducto(?)";
        PreparedStatement function;
        boolean productoExiste=false;
        try {
            function=conexion.prepareStatement(consulta);
            function.setString(1,nombreProducto);
            ResultSet rs =function.executeQuery();
            rs.next();
            productoExiste=rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productoExiste;
    }

    public boolean empleadoNombreYaExiste(String nombre) {
        String editorialNameConsult = "SELECT existeNombreEmpleado(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(editorialNameConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }
    public boolean localNombreYaExiste(String nombre, String apellidos) {
        String completeName = apellidos + ", " + nombre;
        String authorNameConsult = "SELECT existeLocal(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(authorNameConsult);
            function.setString(1, completeName);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }


}
