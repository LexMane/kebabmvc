package base.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboTipoEditorial de la vista.
 * Representan los de eeditorial que existen que existen.
 */


public enum Puesto {
    COCINERO("Cocinero"),
    LIMPIADOR("Limpiador"),
    CAJERO("Cajero");

    private String valor;

    Puesto(String valor) {

        this.valor = valor;
    }

    public String getValor() {

        return valor;
    }
}
