package base.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboGenero de la vista.
 * Representan los géneros literarios que existen.
 */
public enum TipoCarne {
    POLLO("Pollo"),
    TERNERA("Ternera"),
    MIXTO("Mixto");

    private String valor;

    TipoCarne(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
