create database kebabs;
use kebabs;

CREATE TABLE IF NOT EXISTS `Local` (
  `id_local` int auto_increment NOT NULL,
  `nombre_local` varchar(25) NOT NULL,
  `ciudad` varchar(25) NOT NULL,
  `codigo_postal` varchar(5) NOT NULL,
  PRIMARY KEY (`id_local`)
  );
  
CREATE TABLE IF NOT EXISTS `Empleado` (
  `id_empleado` int auto_increment NOT NULL,
  `nombre_empleado` varchar(25) not null,
  `id_local` int NOT NULL,
  `fecha_nacimiento` date not null,
  `salario` double not null,
  `puesto` enum('Cocinero','Limpiador','Cajero'),
  PRIMARY KEY (`id_empleado`),
  FOREIGN KEY (id_local) REFERENCES Local(id_local)
  );

CREATE TABLE IF NOT EXISTS `Kebab` (
  `id_producto` int auto_increment NOT NULL,
  `nombre_producto` varchar(25) not null,
  `id_local` int NOT NULL,
  `precio` double not null,
  `fecha_pedido` date not null,
  `tipo_carne` enum('Pollo','Ternera','Mixto'),
  `salsa` boolean not null,
  `verdura` boolean not null,
  `patatas` boolean not null,
  `id_empleado` int Not null, 
  PRIMARY KEY (`id_producto`),
  FOREIGN KEY (id_local) REFERENCES Local(id_local),
  FOREIGN KEY (id_empleado) REFERENCES Empleado(id_empleado)
  );


delimiter ||
create function existeLocal(f_local varchar(25))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(id_local) from Local)) do
            if  ((select nombre_local from Local where id_local = (i + 1)) like f_local) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreEmpleado(f_nombre varchar(25))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(id_empleado) from Empleado)) do
            if  ((select nombre_empleado from Empleado where id_empleado = (i + 1)) like f_nombre) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeProducto(f_producto varchar(202))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(id_producto) from Kebab)) do
            if  ((select nombre_producto from Kebab where id_producto = (i + 1)) like f_producto) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end; ||
delimiter ;
